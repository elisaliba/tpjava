package com.elisesaliba.java.tp.serie3.exo5;

import com.elisesaliba.java.tp.serie3.exo3.Capitaine;
import com.elisesaliba.java.tp.serie3.exo3.Capitaine.Grade;
import com.elisesaliba.java.tp.serie3.exo4.EquipageCommande;

public class TestBateau {

	public static void main(String[] args) {
		Capitaine commandant = new Capitaine(2000, Grade.CAPITAINE);
		EquipageCommande eCommande = new EquipageCommande(commandant);
		
		Bateau bateau = new Bateau("Bateau1", 100, eCommande);
		
		System.out.println(bateau.toString());
		
		BateauAMoteur bm = new BateauAMoteur("BateauMoteur", 1000, eCommande);
		
		System.out.println(bm.getPropulsion());
	}

}
