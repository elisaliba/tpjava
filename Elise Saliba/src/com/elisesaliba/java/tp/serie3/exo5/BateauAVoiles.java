package com.elisesaliba.java.tp.serie3.exo5;

import com.elisesaliba.java.tp.serie3.exo4.EquipageCommande;

public class BateauAVoiles extends Bateau implements Propulsion {

	public BateauAVoiles(String nom, int tonnage, EquipageCommande eCommandant) {
		super(nom, tonnage, eCommandant);
	}

	public String getPropulsion(){
		return "voiles";
	}
}
