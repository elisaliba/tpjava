package com.elisesaliba.java.tp.serie3.exo5;

import java.util.Arrays;

import com.elisesaliba.java.tp.serie3.exo4.EquipageCommande;

public class Bateau {

	private String nom;
	private int tonnage;
	private EquipageCommande eCommandant;
	
	public Bateau(String nom, int tonnage, EquipageCommande eCommandant){
		this.nom = nom;
		this.tonnage = tonnage;
		this.eCommandant = eCommandant;
	}

	public String getNom() {
		return nom;
	}

	public int getTonnage() {
		return tonnage;
	}

	public EquipageCommande geteCommandant() {
		return eCommandant;
	}

	public void seteCommandant(EquipageCommande eCommandant) {
		this.eCommandant = eCommandant;
	}
	
	public boolean equals(Object o) {
		if (!(o instanceof Bateau))
			return false;
		Bateau bateau = (Bateau) o;
		return nom == bateau.nom;
	}
	
	public String toString() {
		return "Bateau [nom=" + nom + ", tonnage=" + tonnage +
				", nom du Capitaine=" + eCommandant.getCommandant().getNom() +
				", membres de l'�quipage=" + Arrays.toString(eCommandant.getEquipage()) + "]";
	}

}
