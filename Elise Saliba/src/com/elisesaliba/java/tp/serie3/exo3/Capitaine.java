package com.elisesaliba.java.tp.serie3.exo3;

import com.elisesaliba.java.tp.serie1.exo4.Marin;

public class Capitaine extends Marin{

	private Grade grade;
	
	public Capitaine(int salaire, Grade grade) {
		super(salaire);
		this.grade = grade;
	}
	
	public enum Grade {
		BOSCO , CAPITAINE;
	}
	
	public String toString(){
		return super.toString() + ", grade=" + grade;
	}
	
	public Grade getGrade() {
		return grade;
	}

	public void setGrade(Grade grade) {
		this.grade = grade;
	}

	public boolean equals(Object o) {
		if (!(o instanceof Capitaine))
			return false ;
		Capitaine capitaine = (Capitaine) o ;
		return (super.equals(capitaine)) && (grade == capitaine.getGrade());
	}
	
	public int hashCode() {
		int hashCode = 0 ;
		hashCode = super.hashCode() + this.getGrade().hashCode();
		
		return hashCode ;
	}
	
}
