package com.elisesaliba.java.tp.serie3.exo3;

import com.elisesaliba.java.tp.serie1.exo4.Marin;
import com.elisesaliba.java.tp.serie2.exo2.Equipage;
import com.elisesaliba.java.tp.serie3.exo3.Capitaine.Grade;

public class TestCapitaine {

	public static void main(String[] args) {
		
		Capitaine capitaine = new Capitaine(2200, Grade.BOSCO);
		Capitaine capitaine2 = new Capitaine(2200, Grade.BOSCO);
		
		System.out.println(capitaine.equals(capitaine2));
		
		Equipage equipage = new Equipage();
		
		equipage.addMarin(capitaine);
		equipage.addMarin(new Marin(5000));
		
		System.out.println(equipage.toString());

	}

}
