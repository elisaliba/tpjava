package com.elisesaliba.java.tp.serie3.exo4;

import com.elisesaliba.java.tp.serie1.exo4.Marin;
import com.elisesaliba.java.tp.serie3.exo3.Capitaine;
import com.elisesaliba.java.tp.serie3.exo3.Capitaine.Grade;

public class TestEquipageCommande {

	public static void main(String[] args) throws CloneNotSupportedException{
			
		Capitaine capitaine = new Capitaine(1000, Grade.CAPITAINE);
		Marin marin = new Marin(2000);
		
		EquipageCommande eCommandant = new EquipageCommande(capitaine);
			
		// Erreur : constructeur EquipageCommande(Marin) n'existe pas
		// EquipageCommande eMarin = new EquipageCommande(marin);
		
		Marin marinClone = (Marin) marin.clone(); 
			
		System.out.println(marinClone.toString());
	}

}
