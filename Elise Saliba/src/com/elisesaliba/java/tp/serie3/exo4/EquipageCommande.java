package com.elisesaliba.java.tp.serie3.exo4;

import com.elisesaliba.java.tp.serie1.exo4.Marin;
import com.elisesaliba.java.tp.serie2.exo2.Equipage;
import com.elisesaliba.java.tp.serie3.exo3.Capitaine;

public class EquipageCommande extends Equipage {
	private Capitaine commandant;

	public EquipageCommande(Capitaine commandant){
		this.commandant = commandant;
	}
	public Capitaine getCommandant() {
		return commandant;
	}
}
