package com.elisesaliba.java.tp.extra;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

public class Film {
	
	private String titre;
	private int annee;
	private ArrayList<String> acteursCles;
	
	public Film(String titre, int annee, ArrayList<String> acteursCles){
		this.titre = titre;
		this.annee = annee;
		this.acteursCles = acteursCles;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public int getAnnee() {
		return annee;
	}

	public void setAnnee(int annee) {
		this.annee = annee;
	}

	public ArrayList<String> getActeursCles() {
		ArrayList<String> liste = new ArrayList<>();
		Iterator<String> it = acteursCles.iterator();
		while(it.hasNext())
			liste.add(it.next());
		return liste;
	}

	public String toString(){
		return "[" + titre + ", (" + annee + ")]";
	}
	
	public void saveDB(Connection connexion, String table, String titreCol, String anneeCol) throws SQLException{
		String reqFilm = "INSERT INTO " + table + "(" + titreCol + ", " 
				+ anneeCol + ")" + " VALUES('" + titre + "'," + annee + ")";
		Statement statement = connexion.createStatement();
		statement.execute(reqFilm);
	}
}
