package com.elisesaliba.java.tp.extra;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class TestFilms {

	public static void main(String[] args) throws SQLException {
		Connection con = null;
		try {
			Class c = Class.forName("com.mysql.jdbc.Driver");
			Driver pilote = (Driver) c.newInstance();
			DriverManager.registerDriver(pilote);

			String protocole =  "jdbc:mysql:";
			String ip =  "localhost";
			String port =  "3306";

			String nomBase =  "MOVIES"; 

			String conString = protocole +  "//" + ip +  ":" + port +  "/" + nomBase ;

			String nomConnexion =  "root"; 
			String motDePasse =  "root";

			con = DriverManager.getConnection(
					conString, nomConnexion, motDePasse) ;

//			createDbTables(con);

			String nomFichier = "movies-test.txt";
			TreeMap<String, Film> retr = lireFilms(nomFichier);

//			Acteur.saveAllDB(con, "ACTEURS", "NOM", "PRENOM");

			PreparedStatement psFilmsActeurs = con.prepareStatement(
					"INSERT INTO FILMS_ACTEURS(TITRE, ANNEE, NOM, PRENOM)"
							+ " VALUES (?,?,?,?)") ;

			for (Map.Entry<String, Film> entry : retr.entrySet()) {
				Film film = entry.getValue();
				ArrayList<String> cles = film.getActeursCles();
			//	System.out.println(film.toString());
/*
				film.saveDB(con, "FILMS", "TITRE", "ANNEE");
				for(String cle : cles){
					psFilmsActeurs.setString(1, entry.getValue().getTitre());
					psFilmsActeurs.setInt(2, entry.getValue().getAnnee());
					psFilmsActeurs.setString(3, Acteur.getActeursMap().get(cle).getNom());
					psFilmsActeurs.setString(4, Acteur.getActeursMap().get(cle).getPrenom());
					psFilmsActeurs.executeUpdate();
				}
	*/			
			}

			String nomActeur = "Hancock";
			String prenomActeur = "John";
			ArrayList<Film> films = new ArrayList<>();

			for (Map.Entry<String, Film> entry : retr.entrySet()) {
				Film film = entry.getValue();
				ArrayList<String> cles = film.getActeursCles();
				for(String cle : cles){
					if(cle.equals(prenomActeur+nomActeur))
						films.add(film);
				}
			}
			
			System.out.println(films.toString());
			Statement st = con.createStatement();
			String requette = "SELECT TITRE, ANNEE "
					+ "FROM FILMS_ACTEURS "
					+ "WHERE NOM = '" + prenomActeur + "' AND "
							+ "PRENOM = '" + nomActeur + "'";
			ResultSet rs = st.executeQuery(requette);
			String str = "";
			while(rs.next()){
				 str += rs.getString("TITRE") + ", (" + rs.getInt("ANNEE") + ")\n";				
			}
			System.out.println(str);

		}  catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(con!=null) {
				con.close();
			}
		}

	}

	public static void createDbTables(Connection con)throws SQLException{
		Statement createTablesStatement = null;
		String createTableFilms = "CREATE TABLE IF NOT EXISTS FILMS("
				+ "TITRE VARCHAR(200) NOT NULL,"
				+ "ANNEE INT NOT NULL,"
				+ "PRIMARY KEY(TITRE, ANNEE)"
				+ ")";

		String createTableActeurs = "CREATE TABLE IF NOT EXISTS ACTEURS("
				+ "NOM VARCHAR(200) NOT NULL,"
				+ "PRENOM VARCHAR(200) NOT NULL,"
				+ "PRIMARY KEY(NOM, PRENOM)"
				+ ")";

		String createTableFilmsActeurs = "CREATE TABLE IF NOT EXISTS FILMS_ACTEURS("
				+ "TITRE VARCHAR(200) NOT NULL REFERENCES FILM(TITRE),"
				+ "ANNEE INT NOT NULL REFERENCES FILM(ANNEE),"
				+ "NOM VARCHAR(200) NOT NULL REFERENCES ACTEUR(NOM),"
				+ "PRENOM VARCHAR(200) NOT NULL REFERENCES ACTEUR(PRENOM),"
				+ "PRIMARY KEY(TITRE, ANNEE, NOM, PRENOM)"
				+ ")";
		try {
			createTablesStatement = con.createStatement();
			createTablesStatement.execute(createTableFilms);
			System.out.println("Table \"FILMS\" is created!");
			createTablesStatement.execute(createTableActeurs);
			System.out.println("Table \"ACTEURS\" is created!");
			createTablesStatement.execute(createTableFilmsActeurs);
			System.out.println("Table \"FILMS_ACTEURS\" is created!");
		} catch(SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if(createTablesStatement != null) {
				createTablesStatement .close();
			}
		}
	}

	public static TreeMap<String, Film> lireFilms(String nomFichier){
		String titre, nomActeur, prenomActeur;
		titre = nomActeur = prenomActeur = null;
		int annee = 0;
		Boolean premiere;
		Film film;
		ArrayList<String> acteurs;
		TreeMap<String, Film> resultat = new TreeMap<>();
		FileReader fr = null ;
		BufferedReader br = null;

		try {
			String line = null;
			fr = new FileReader(nomFichier) ;
			br = new BufferedReader(fr);

			while ((line = br.readLine()) != null) {
				acteurs = new ArrayList<>();
				StringTokenizer st = new StringTokenizer(line, "/");
				premiere = true;
				while (st.hasMoreElements()) {
					if(premiere){
						String para = st.nextToken();
						int index = para.indexOf("(");
						if (index != -1){
							titre = para.substring(0 , index-1);
							annee = Integer.parseInt(para.substring(index+1, index+5)); 
						}
						premiere = false;
					} else {
						String token = st.nextToken();
						int ind = token.indexOf(",");
						if(ind!= -1){
							prenomActeur = token.substring(0, ind);
							nomActeur = token.substring(ind+2);
						} else {
							prenomActeur = token.substring(0);
							nomActeur = "";
						}
						Acteur acteur = new Acteur(nomActeur, prenomActeur);
						acteurs.add(nomActeur+prenomActeur);
					}	
				}
				film = new Film(titre, annee, acteurs);
				resultat.put(titre+annee, film);
			}
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		} finally {
			if (fr != null) {
				try {
					br.close();
					fr.close() ;
				} catch (IOException e) {
				}
			}
		}
		return resultat;
	}

}
