package com.elisesaliba.java.tp.extra;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.StringTokenizer;
import java.util.TreeMap;


public class LireFilms {

	public TreeMap<String, Film> lireFilms(String nomFichier){
		String titre, nomActeur, prenomActeur;
		titre = nomActeur = prenomActeur = null;
		int annee = 0;
		Boolean premiere;
		Film film;
		ArrayList<String> acteurs;
		TreeMap<String, Film> resultat = new TreeMap<>();
		FileReader fr = null ;
		BufferedReader br = null;

		try {
			String line = null;
			fr = new FileReader(nomFichier) ;
			br = new BufferedReader(fr);

			while ((line = br.readLine()) != null) {
				acteurs = new ArrayList<>();
				StringTokenizer st = new StringTokenizer(line, "/");
				premiere = true;
				while (st.hasMoreElements()) {
					if(premiere){
						String para = st.nextToken();
						int index = para.indexOf("(");
						if (index != -1){
							titre = para.substring(0 , index-1);
							annee = Integer.parseInt(para.substring(index+1, index+5)); 
						}
						premiere = false;
					} else {
						String token = st.nextToken();
						int ind = token.indexOf(",");
						if(ind!= -1){
							nomActeur = token.substring(0, ind);
							prenomActeur = token.substring(ind+2);
						} else {
							prenomActeur = token.substring(0);
							nomActeur = "";
						}
						Acteur acteur = new Acteur(nomActeur, prenomActeur);
						acteurs.add(nomActeur+prenomActeur);
					}	
				}
				film = new Film(titre, annee, acteurs);
				resultat.put(titre+annee, film);
			}
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		} finally {
			if (fr != null) {
				try {
					br.close();
					fr.close() ;
				} catch (IOException e) {
				}
			}
		}
		return resultat;
	}
}
