package com.elisesaliba.java.tp.extra;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.TreeMap;

public class Acteur {

	private String nom, prenom;
	private static TreeMap<String, Acteur> acteursMap = new TreeMap<>();
	
	public Acteur(String nom, String prenom){
		this.nom = nom;
		this.prenom = prenom;
		acteursMap.put(nom+prenom, this);
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		return result;
	}
	
	public boolean equals(Object o) {
		if (!(o instanceof Acteur))
			return false ;
		Acteur acteur = (Acteur) o ;
		return nom.equals(acteur.nom) && prenom.equals(acteur.prenom);
	}
	
	public String toString(){
		return "{" + nom + " " + prenom + "}";
	}
	
	public static TreeMap<String, Acteur> getActeursMap(){
		return acteursMap;
	}
	
	public void saveDB(Connection connexion, String table, String nomCol, String prenomCol) throws SQLException{
		String reqActeur = "INSERT INTO " + table + "(" + nomCol + ", " 
				+ prenomCol + ")" + " VALUES ('" + nom + "','" + prenom + "')";
		Statement statement = connexion.createStatement();
		statement.execute(reqActeur);
	}
	
	public static void saveAllDB(Connection connexion, String table, String nomCol, String prenomCol) throws SQLException{
		PreparedStatement psActeurs = connexion.prepareStatement("INSERT INTO " + table + "(" + nomCol + ", " 
				+ prenomCol + ")" + " VALUES (?,?)");
		for(Map.Entry<String, Acteur> entry : acteursMap.entrySet()) {
			psActeurs.setString(1, entry.getValue().getNom());
			psActeurs.setString(2, entry.getValue().getPrenom());
			psActeurs.executeUpdate();
		}
	}
}
