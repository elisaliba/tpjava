package com.elisesaliba.java.tp.serie1.exo5;
import java.util.Arrays;
import com.elisesaliba.java.tp.serie1.exo4.Marin;

public class MarinUtil {
	
	public MarinUtil(){
	
	}
	
	public static void augmenteSalaire (Marin[] marins, int pourcentage){
		for (int i = 0; i < marins.length; i++)
			marins[i].setSalaire(marins[i].getSalaire() + marins[i].getSalaire()*pourcentage/100); 
	}
	
	public static int getMaxSalaire (Marin[] marins){
		int maxSalaire = 0;
		for (int i = 0; i < marins.length; i++)
			if(marins[i].getSalaire() > maxSalaire) 
				maxSalaire = marins[i].getSalaire();
		return maxSalaire;
	}
	
	public static double getMoyenneSalaire (Marin[] marins){
		double moyenneSalaire = 0;
		for (int i = 0; i < marins.length; i++)
			moyenneSalaire += marins[i].getSalaire();
		return moyenneSalaire/marins.length;
	}
	
	public static int getMedianeSalaire (Marin[] marins){
		int medianeSalaire = 0;
		int[] salaireCroissant = new int[marins.length];
		for (int i = 0; i < marins.length; i++)
			salaireCroissant[i] = marins[i].getSalaire();
		Arrays.sort(salaireCroissant);
		
		if(marins.length%2 == 0) {
			medianeSalaire = (salaireCroissant[((marins.length)/2)] + salaireCroissant[((marins.length)/2 + 1)])/2;
		} 
		else
			medianeSalaire = salaireCroissant[(marins.length -1)/2];
		
		return medianeSalaire;
	}
	
}
