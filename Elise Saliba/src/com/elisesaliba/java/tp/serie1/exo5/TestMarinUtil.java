package com.elisesaliba.java.tp.serie1.exo5;

import com.elisesaliba.java.tp.serie1.exo4.Marin;

public class TestMarinUtil {

	public static void main(String[] args) {
	
		int maxSalaire, medianeSalaire;
		double moyenneSalaire;
		
		Marin marins[] = new Marin[5];
		
		marins[0] = new Marin("Elise", "Saliba", 2000);
		marins[1] = new Marin("Anais", "Berthereau", 2500);
		marins[2] = new Marin("Helene", "Diop", 3000);
		marins[3] = new Marin("Niouma", "Sakho", 3500);
		marins[4] = new Marin("Fatou", "Fall", 4000);
		
		MarinUtil.augmenteSalaire(marins, 10);

		maxSalaire = MarinUtil.getMaxSalaire(marins);
		
		moyenneSalaire = MarinUtil.getMoyenneSalaire(marins);
		
		medianeSalaire = MarinUtil.getMedianeSalaire(marins);
		
		for (int i = 0; i < marins.length; i++)
			System.out.println(marins[i].toString());
		
		System.out.println("\nLe salaire du marin le mieux pay� est : " + maxSalaire);
		System.out.println("\nLa moyenne des salaires des marins est : " + moyenneSalaire);
		System.out.println("\nLa mediane des salaires des marins est : " + medianeSalaire);
	}

}
