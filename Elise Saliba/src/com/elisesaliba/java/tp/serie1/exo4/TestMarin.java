package com.elisesaliba.java.tp.serie1.exo4;

public class TestMarin {

	public static void main(String[] args) {
		Marin m1 = new Marin("Saliba", "Elise", 2500);
		Marin m2 = new Marin("Saliba", "Elise", 2500);
		Marin m3 = new Marin("Aoun", "Georges", 2000);
		
		if( m1.equals(m2) )
			System.out.println("m1 = m2");
		if( m1.equals(m3) )
			System.out.println("m1 = m2 = m3");

		System.out.println(m1.hashCode() + "\n" + m2.hashCode() + "\n" + m3.hashCode());
	}

}
