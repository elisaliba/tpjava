package com.elisesaliba.java.tp.serie1.exo4;

import java.io.Serializable;

public class Marin implements Cloneable, Comparable<Marin>, Serializable {

	private String nom, prenom;
	private int salaire;

 // Serie 3 
	/*
	public Marin(int salaire){
		nom = "Nom";
		prenom = "Prenom";
		this.salaire = salaire;
	}

	public String getNom(){
		return nom;
	}

	public String getPrenom(){
		return prenom;
	}

	public void setSalaire(int salaire){
		this.salaire = salaire;
	}

	public int getSalaire(){
		return salaire;
	}
	
	public String toString() {
		return "Marin : salaire=" + salaire;
	}
	
	public boolean equals(Object o) {
		if (!(o instanceof Marin))
			return false ;
		Marin marin = (Marin) o ;
		return salaire == marin.salaire ;
	}
	
	public int hashCode() {
		int hashCode = 17 ;
		hashCode = 31 * hashCode + salaire;
		return hashCode ;
	}
	
	// Serie 3 exercice 4 -> clonage
	public Marin clone() throws CloneNotSupportedException {
		return (Marin) super.clone();
	}
	*/
	
	// Autre series

	public Marin(String nom, String prenom, int salaire){
		this.nom = nom;
		this.prenom = prenom;
		this.salaire = salaire;
	}

	public Marin(String nom, int salaire){
		this(nom,"",salaire);
	}

	public void setNom(String nom){
		this.nom = nom;
	}

	public String getNom(){
		return nom;
	}

	public void setPrenom(String prenom){
		this.prenom = prenom;
	}

	public String getPrenom(){
		return prenom;
	}

	public void setSalaire(int salaire){
		this.salaire = salaire;
	}

	public int getSalaire(){
		return salaire;
	}

	public int augmenteSalaire(int augmentation){
		return salaire += augmentation;
	}

	public String toString() {
		return "Marin [nom=" + nom + ", prenom=" + prenom + ", salaire=" + salaire + "]";
	}

	public boolean equals(Object o) {
		if (!(o instanceof Marin))
			return false ;
		Marin marin = (Marin) o ;
		return nom.equals(marin.nom) && prenom.equals(marin.prenom) &&
		salaire == marin.salaire ;
	}
	
	public int hashCode() {
		int hashCode = 17 ;
		hashCode = 31 * hashCode + ((nom == null) ? 0 : nom.hashCode());
		hashCode = 31 * hashCode + ((prenom == null) ? 0 : prenom.hashCode());
		hashCode = 31 * hashCode + salaire;
		
		return hashCode ;
	}

	@Override
	public int compareTo(Marin o) {
		if (getNom().equals(o.getNom())) {
			return getPrenom().compareTo(o.getPrenom()) ;
		} else {
			return getNom().compareTo(o.getNom()) ;
		}
	}

}
