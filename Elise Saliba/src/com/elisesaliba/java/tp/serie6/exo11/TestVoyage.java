package com.elisesaliba.java.tp.serie6.exo11;

import java.util.ArrayList;

public class TestVoyage {

	public static void main(String[] args) {
		Voyage v1 = new Voyage("Paris", "Italie", 2);
		Voyage v2 = new Voyage("Paris", "Finland", 3);
		Voyage v3 = new Voyage("Italie", "Paris", 1);
		Voyage v4 = new Voyage("Grece", "Italie", 2);	
		
		System.out.println(v1.hashCode());
		System.out.println(v3.hashCode());
		System.out.println("Le voyage le plus court de Paris : " + 
		Voyage.getPlusCourtAuDepartDe("Paris").toString());
		ArrayList<Voyage> liste = new ArrayList<>();
		liste = Voyage.getVoyageAuDepartDe("Paris");
		
		System.out.println("Les voyages de Paris :\n");
		for(Voyage v : liste)
			System.out.println(v.toString());
	// Si le constructeur est prive, pour creer une nouvelle instance de Voyage :
	   System.out.println(Voyage.newInstance("Beyrouth", "Paris", 1).toString());
	}

}
