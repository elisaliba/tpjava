package com.elisesaliba.java.tp.serie6.exo11;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class Voyage {

	private String depart, destination;
	private int nombreJours;
	private static ArrayList<Voyage> listeVoyages = new ArrayList<>();

	public Voyage(String depart, String destination, int nombreJours){
		this.depart = depart;
		this.destination = destination;
		this.nombreJours = nombreJours;
		listeVoyages.add(this);
	}

	public String getDepart() {
		return depart;
	}

	public String getDestination() {
		return destination;
	}

	public int getNombreJours() {
		return nombreJours;
	}

	public boolean equals(Object obj) {
		if (!(obj instanceof Voyage))
			return false ;
		Voyage voyage = (Voyage) obj ;
		return (depart.equals(voyage.depart) && destination.equals(voyage.destination)) ||
				(depart.equals(voyage.destination) && destination.equals(voyage.depart)) ;
	}

	public int hashCode() {
		final int prime = 31;
		int hashcode = 1;
		if(depart.compareTo(destination) <0)
		{
		hashcode = prime * hashcode + ((depart == null) ? 0 : depart.hashCode());
		hashcode = prime * hashcode + ((destination == null) ? 0 : destination.hashCode());
		}
		else if(depart.compareTo(destination) >0)
		{
			hashcode = prime * hashcode + ((destination == null) ? 0 : destination.hashCode());
			hashcode = prime * hashcode + ((depart == null) ? 0 : depart.hashCode());
		}
			
		return hashcode;
	}

	public static Voyage getVoyage(String dep, String dest){
		Voyage voyage = null;
		for(Voyage v : listeVoyages)
			if((v.getDepart() == dep) && (v.getDestination() == dest))
				voyage = v;
		return voyage;
	}

	public static ArrayList<Voyage> getVoyageAuDepartDe(String dep){
		ArrayList<Voyage> resultat = new ArrayList<Voyage>();
		for(Voyage v : listeVoyages)
			if((v.getDepart() == dep))
				resultat.add(v);
		
		return resultat;
	}

	public static Voyage getPlusCourtAuDepartDe(String dep){
		ArrayList<Voyage> voyages = new ArrayList<>();
		voyages = getVoyageAuDepartDe(dep);
		int nbreJours = voyages.get(0).getNombreJours();
		Voyage voyage = voyages.get(0);
		
		for(Voyage v : voyages){
			if(v.getNombreJours()< nbreJours){
				nbreJours = v.getNombreJours();
				voyage = v;
			}
		}
		return voyage;
	}
	
	public static Voyage newInstance(String dep, String arr, int nbreJours){
		Voyage voyage = null;
		for(Voyage v : listeVoyages){
			if((v.getDepart() == dep) && (v.getDestination() == arr))
				voyage = v;
		}
		if(voyage == null){
			voyage = new Voyage(dep, arr, nbreJours);
			listeVoyages.add(voyage);
		}

		return voyage;
	}
	
	public String toString(){
		return "[Depart : " + this.getDepart() + ", destination : " + this.getDestination()
		+ ", duree : " + this.getNombreJours() + " jours]";
	}

}
