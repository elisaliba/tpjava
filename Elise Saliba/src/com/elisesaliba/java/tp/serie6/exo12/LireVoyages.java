package com.elisesaliba.java.tp.serie6.exo12;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.elisesaliba.java.tp.serie6.exo11.Voyage;

public class LireVoyages {

	public static void main(String[] args) {
		String nomFichier = "Voyages.txt";
		FileReader fr = null ;
		BufferedReader br = null;
		try {
			String line = null;
			String param[] = null;
			ArrayList<Voyage> liste = new ArrayList<>();
			fr = new FileReader(nomFichier) ;
			br = new BufferedReader(fr); // pour lire ligne par ligne

			while ((line = br.readLine()) != null) {
				System.out.println(line);
				param = line.split(" ");
				liste.add(new Voyage(param[0].substring(1, param[0].length()-1),
						param[1].substring(1, param[1].length()-1),Integer.parseInt(param[2])));
			}
			for(Voyage v : liste)
				System.out.println(v.toString());
			
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		} finally {
			if (fr != null) {
				try {
					br.close();
					fr.close() ;
				} catch (IOException e) {
				}
			}
		}

	}

}
