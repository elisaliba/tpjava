package com.elisesaliba.java.tp.serie0.exo3;

public class TestPalindromes {
	
	public static void main(String[] args) {
		Palindromes p = new Palindromes();
		for(int i = 10; i < 150000; i++ ){
			if (p.checkPremier(i))
				if (p.checkPalindrome(i))
					System.out.println(i + "\n");
		}
	}

}
