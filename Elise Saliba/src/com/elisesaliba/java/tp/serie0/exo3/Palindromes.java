package com.elisesaliba.java.tp.serie0.exo3;

public class Palindromes {

	public boolean checkPalindrome(int nombre) {
		String nombreReverse = new StringBuffer(nombre+"").reverse().toString();
		if(nombreReverse.equals(nombre+""))
			return true;
		else
			return false;
	}

	public boolean checkPremier(int nombre) {
		int racineNombre;

		racineNombre = (int) Math.sqrt(nombre);

		for(int i = racineNombre; i > 1; i--){
			if(nombre % i == 0) {
				return false;
			}
		}
		return true;
	}
}
