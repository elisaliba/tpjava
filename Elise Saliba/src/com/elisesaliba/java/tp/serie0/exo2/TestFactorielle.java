package com.elisesaliba.java.tp.serie0.exo2;

import java.math.BigInteger;

public class TestFactorielle {

	public static void main(String[] args) {
		
		Factorielle fact = new Factorielle();
		System.out.println("Factorielle de 10 = " +
		fact.factorielle(new BigInteger("30")));

	}

}
