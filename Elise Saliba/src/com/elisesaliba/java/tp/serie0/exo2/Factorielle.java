package com.elisesaliba.java.tp.serie0.exo2;

import java.math.BigInteger;

public class Factorielle {
	

public int factorielle(int valeur) {
	
		int fact;
		fact = 1;
		
		for(int i = 0; i < valeur; i++) {
			fact = fact * (valeur-i);
		}
		
		return fact;
	}
	
public BigInteger factorielle(BigInteger valeur) {
		
		BigInteger fact;
		fact = BigInteger.ONE;
		
		for(BigInteger i = BigInteger.ZERO; i.compareTo(valeur)==-1; i = i.add(BigInteger.ONE)) {
			fact = fact.multiply(valeur.subtract(i));
		}
		
		return fact;
	}
	
	
}
