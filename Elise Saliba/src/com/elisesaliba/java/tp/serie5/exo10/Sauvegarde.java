package com.elisesaliba.java.tp.serie5.exo10;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import com.elisesaliba.java.tp.serie1.exo4.Marin;

public class Sauvegarde implements Serializable{

	public Sauvegarde(){

	}

	public static void sauveFichierTexte(String nomFichier, Marin marin){
		File fichier = new File(nomFichier) ;
		Writer writer = null ;
		try {
			writer = new FileWriter(fichier, true) ; // true to append
			writer.write(marin.getNom() + "|" + marin.getPrenom()
			+"|" + marin.getSalaire() + "\n") ;
		} catch (IOException e) {
			System.out.println("Erreur " + e.getMessage()) ;
			e.printStackTrace() ;
		} finally {
			if (writer != null) {
				try {
					writer.close() ;
				} catch (IOException e) {
					System.out.println("Erreur " + e.getMessage()) ;
					e.printStackTrace() ;
				}
			}
		}
	}

	public static List<Marin> lisFichierTexte(String nomFichier){
		List<Marin> listeMarins = new ArrayList<Marin>();
		Marin marin;
		String param[];
		FileReader fr = null ;
		BufferedReader br = null;
		try {
			String line = null;
			fr = new FileReader(nomFichier) ;
			br = new BufferedReader(fr); // pour lire ligne par ligne

			while ((line = br.readLine()) != null) {
				param = line.split("\\|");
				marin = new Marin(param[0], param[1], Integer.parseInt(param[2]));
				listeMarins.add(marin);
			}
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		} finally {
			if (fr != null) {
				try {
					br.close();
					fr.close() ;
				} catch (IOException e) {
				}
			}
		}
		return listeMarins;

	}

	public static void sauveChampBinaire(String nomFichier, Marin marin) throws IOException{
		File fichier = new File(nomFichier);
		OutputStream os = null;
		DataOutputStream dos = null;

		try {
			os = new FileOutputStream(fichier, true);
			dos = new DataOutputStream(os);
			dos.writeUTF(marin.getNom());
			dos.writeUTF(marin.getPrenom());
			dos.writeInt(marin.getSalaire());

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(os!= null){
				dos.close();
				os.close();
			}
		}

	}

	public static List<Marin> lisChampBinaire(String nomFichier) throws IOException{
		File fichier = new File(nomFichier);
		InputStream is = null;
		DataInputStream dis = null;
		List<Marin> listeMarins = new ArrayList<Marin>();
		String nom, prenom;
		int salaire;
		try {
			is = new FileInputStream(fichier);
			dis = new DataInputStream(is);

			while(dis.available()>0){
				nom = dis.readUTF();
				prenom = dis.readUTF();
				salaire = dis.readInt();
				listeMarins.add(new Marin(nom, prenom, salaire));
			}


		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(is!= null)
				dis.close();
			is.close();
		}

		return listeMarins;
	}

	public static void sauveObjet(String nomFichier, Marin[] marin) throws IOException{
		ObjectOutputStream oos;
		File fichier;

		try {
			fichier = new File(nomFichier);
			oos = new ObjectOutputStream(new FileOutputStream(fichier));

			for(int i = 0; i <marin.length; i++)
				oos.writeObject(marin[i]);

			oos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static List<Marin> lisObjet(String nomFichier) throws ClassNotFoundException, FileNotFoundException, IOException{
		File fichier = new File(nomFichier);
		List<Marin> liste = new ArrayList<Marin>();
		ObjectInputStream ois = null;
		int count = 0;
		Object o= null;
		try {
			ois = new ObjectInputStream(new FileInputStream(fichier));
			while(( o = ois.readObject()) != null) {
				count++;
				liste.add((Marin) o);
			}
		} 
		catch (EOFException e){
			return liste;
		}
		finally {
			if (ois != null) {
				try {
					ois.close() ;
				} catch (IOException e) {
				}
			}
		}
		System.out.println(count);
		return liste;
	}

	public static long getLength(String nomFichier){
		File fichier;
		try {
			fichier = new File(nomFichier);
			if(fichier.exists())
				return fichier.length();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

}
