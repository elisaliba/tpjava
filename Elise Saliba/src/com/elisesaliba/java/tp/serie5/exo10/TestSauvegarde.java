package com.elisesaliba.java.tp.serie5.exo10;

import java.io.IOException;

import com.elisesaliba.java.tp.serie1.exo4.Marin;

public class TestSauvegarde {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		String nomFichier;
		Marin m1, m2;
		Marin[] m;
		m1 = new Marin("Nom1", "Prenom1", 500000);
		m2 = new Marin("Nom2", "Prenom2", 500000);
		
		nomFichier = "Exo9Texte.txt";
		Sauvegarde.sauveFichierTexte(nomFichier, m1);
		Sauvegarde.sauveFichierTexte(nomFichier, m2);
		System.out.println(Sauvegarde.lisFichierTexte(nomFichier));
		System.out.println(Sauvegarde.getLength(nomFichier));
		
		nomFichier = "Exo9Objet.txt";
		m = new Marin[2];
		m[0] = m1;
		m[1] = m2;
		Sauvegarde.sauveObjet(nomFichier, m);
		System.out.println(Sauvegarde.lisObjet(nomFichier));
		System.out.println(Sauvegarde.getLength(nomFichier));
		
		nomFichier = "Exo9Binaire.txt";
		
		Sauvegarde.sauveChampBinaire(nomFichier, m1);
		Sauvegarde.sauveChampBinaire(nomFichier, m2);
		System.out.println(Sauvegarde.lisChampBinaire(nomFichier));
		System.out.println(Sauvegarde.getLength(nomFichier));
	
	}

}
