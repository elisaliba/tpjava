package com.elisesaliba.java.tp.serie4.exo9;

import com.elisesaliba.java.tp.serie1.exo4.Marin;
import com.elisesaliba.java.tp.serie2.exo2.Equipage;

public class TestTreeMap {

	public static void main(String[] args) {
		
		Equipage equipage = new Equipage();
		Marin m1, m2, m3, m4, m5;
		
		m1 = new Marin("saliba", "Elise", 1000);
		m2 = new Marin("Saliba", "Elise", 3000);
		m3 = new Marin("Diop", "Helene", 1000);
		m4 = new Marin("Diop", "Ella", 1000);
		m5 = new Marin("Diop", "Ella", 5000);
		
		equipage.addMarin(m1);
		equipage.addMarin(m2);
		equipage.addMarin(m3);
		equipage.addMarin(m4);
		equipage.addMarin(m5);
		
		System.out.println(equipage.getMarinByName("Saliba"));
		
		System.out.println(equipage.getMarinByName("Sali"));
		
		System.out.println(equipage.toString());
	}

}
