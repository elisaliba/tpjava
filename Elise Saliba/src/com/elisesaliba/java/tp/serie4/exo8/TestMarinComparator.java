package com.elisesaliba.java.tp.serie4.exo8;

import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

import com.elisesaliba.java.tp.serie1.exo4.Marin;

public class TestMarinComparator {

	public static void main(String[] args) {
		Marin m1, m2, m3, m4, m5;
		MarinComparator marinsComparator = new MarinComparator();
		m1 = new Marin("saliba", "Elise", 1000);
		m2 = new Marin("Saliba", "Elise", 3000);
		m3 = new Marin("Diop", "Helene", 1000);
		m4 = new Marin("Diop", "Ella", 1000);
		m5 = new Marin("Diop", "Ella", 5000);

		System.out.println(marinsComparator.compare(m1, m2));
		System.out.println(marinsComparator.compare(m2, m1));
		System.out.println(marinsComparator.compare(m2, m3));
		System.out.println(marinsComparator.compare(m3, m4));
		System.out.println(marinsComparator.compare(m4, m5));
		
		System.out.println(m1.compareTo(m2));
		System.out.println(m2.compareTo(m1));
		System.out.println(m2.compareTo(m3));
		System.out.println(m3.compareTo(m4));
		System.out.println(m4.compareTo(m5));
		
		// on obtient le meme resultat
		
		SortedSet<Marin> marinSet = new TreeSet<Marin>(new Comparator<Marin>() {
			public int compare(Marin m1, Marin m2) {
				if (m1.getNom().equals(m2.getNom())) {
					return m1.getPrenom().compareTo(m2.getPrenom()) ;
				} else {
					return m1.getNom().compareTo(m2.getNom()) ;
					}
				}
			}) ;
		
		if(!marinSet.add(m1))
			System.out.println("L'�l�ment " + m1.getNom() + " " + m1.getPrenom() + " existe d�j�.");
		if(!marinSet.add(m2))
			System.out.println("L'�l�ment " + m2.getNom() + " " + m2.getPrenom() + " existe d�j�.");
		if(!marinSet.add(m3))
			System.out.println("L'�l�ment " + m3.getNom() + " " + m3.getPrenom() + " existe d�j�.");
		if(!marinSet.add(m4))
			System.out.println("L'�l�ment " + m4.getNom() + " " + m4.getPrenom() + " existe d�j�.");
		if(!marinSet.add(m5))
			System.out.println("L'�l�ment " + m5.getNom() + " " + m5.getPrenom() + " existe d�j�.");
		
		for (Marin m : marinSet) 
			System.out.println(m);
	}

}
