package com.elisesaliba.java.tp.serie4.exo8;

import java.util.Comparator;

import com.elisesaliba.java.tp.serie1.exo4.Marin;

public class MarinComparator implements Comparator<Marin> {

	@Override
	public int compare(Marin m1, Marin m2) {
		if (m1.getNom().equals(m2.getNom())) {
			return m1.getPrenom().compareTo(m2.getPrenom()) ;
		} else {
			return m1.getNom().compareTo(m2.getNom()) ;
			}
	}

	
}
