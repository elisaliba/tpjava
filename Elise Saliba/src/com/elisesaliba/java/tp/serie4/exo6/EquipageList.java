package com.elisesaliba.java.tp.serie4.exo6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.elisesaliba.java.tp.serie1.exo4.Marin;

public class EquipageList {
	
	private List<Marin> marins;
	private final int MAX = 5;	
	
	public EquipageList () {
		marins = new ArrayList<Marin>(MAX) ;
	}

	public int getNombreMarins(){
		return marins.size();
	}

	public boolean addMarin(Marin marin){
		return marins.add(marin);
	}

	@Override
	public String toString() {
		String str = "";
		
		for (Marin m : marins)
			str+= m.toString() + "\n";
	
		return str;
	}

	public boolean isMarinPresent(Marin marin){
		return marins.contains(marin);
	}

	public boolean removeMarin(Marin marin){
		return marins.remove(marin);
	}

	public List<Marin> getEquipage() {
		List<Marin> copieMarins = new ArrayList<Marin>(this.getNombreMarins());
		
		for (int i = 0; i < this.getNombreMarins(); i++)
			copieMarins.add((Marin) marins.get(i));
		
		return copieMarins;
	}

	public void clear(){
		marins.clear();
	}

	public boolean addAllEquipage(EquipageList equipage){
		return marins.addAll(equipage.getEquipage());
	}

	public boolean equals(EquipageList equipageAutre){
		if(this.getNombreMarins() != equipageAutre.getNombreMarins())
			return false;

		for(int i = 0; i < equipageAutre.getNombreMarins(); i++)
			if(!this.isMarinPresent(equipageAutre.getEquipage().get(i)))
				return false;
		return true;
	}
	
	public int hashCode() {
		int hashCode = 17;
		int[] marinsHashCode = new int[this.getNombreMarins()];
		for(int i = 0; i < this.getNombreMarins(); i++)
			marinsHashCode[i] = this.getEquipage().get(i).hashCode();
		
		Arrays.sort(marinsHashCode);
		
		System.out.println(Arrays.toString(marinsHashCode));
		
		for(Integer i : marinsHashCode)
			hashCode = 31 * hashCode + i;
		
		return hashCode ;
	}

}
