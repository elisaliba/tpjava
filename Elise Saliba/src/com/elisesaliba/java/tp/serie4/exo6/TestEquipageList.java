package com.elisesaliba.java.tp.serie4.exo6;

import com.elisesaliba.java.tp.serie1.exo4.Marin;
import com.elisesaliba.java.tp.serie2.exo2.Equipage;

public class TestEquipageList {

	public static void main(String[] args) {
	
		Marin m1, m2, m3, m4;
		
		Equipage equipage = new Equipage();
		m1 = new Marin("nom1", "prenom1", 1000);
		m2 = new Marin("nom2", "prenom2", 2000);
		m3 = new Marin("nom3", "prenom3", 3000);
		m4 = new Marin("nom4", "prenom4", 4000);
		
		System.out.println("Capacite de l'equipage avant ajout : " + equipage.getNombreMarins());
		
		if(equipage.addMarin(m1))
			System.out.println("Marin 1 ajout�.");
		if(equipage.addMarin(m2))
			System.out.println("Marin 2 ajout�.");
		if(equipage.addMarin(m3))
			System.out.println("Marin 3 ajout�.");
		if(equipage.addMarin(m4))
			System.out.println("Marin 4 ajout�.");

		System.out.println("Capacite de l'equipage apr�s ajout : " + equipage.getNombreMarins());
		
		System.out.println(equipage.toString());
		
		if(equipage.isMarinPresent(m2))
			System.out.println("Marin 2 est pr�sent.");
		else
			System.out.println("Marin 2 n'est pas pr�sent.");
		
		Marin m5 = new Marin("nom5", "prenom5", 5000);
		
		if(equipage.isMarinPresent(m5))
			System.out.println("Marin 5 est pr�sent.");
		else
			System.out.println("Marin 5 n'est pas pr�sent.");
		
		if(equipage.removeMarin(m1))
			System.out.println("Marin 1 a �t� enlev�.");
		else
			System.out.println("Marin 1 n'a pas �t� enlev�.");
		
		System.out.println("Capacite de l'equipage apr�s suppression du marin 1 : " + equipage.getNombreMarins());
		System.out.println(equipage.toString());
		
		if(equipage.addAllEquipage(equipage))
			System.out.println(equipage.toString());
		else
			System.out.println("La taille de l'�quipage 1 d�passe la capacit� de l'�quipage courant.");
		
		Equipage equipage2 = new Equipage();
		if(!equipage2.addMarin(new Marin("nom6", "prenom6", 6000)))
			System.out.println("Marin existe dej� ou equipage est plein.");
		if(!equipage2.addMarin(new Marin("nom7", "prenom7", 7000)))
			System.out.println("Marin existe dej� ou equipage est plein.");
		
		System.out.println(equipage2.toString());
		
		if(equipage.addAllEquipage(equipage2))
			System.out.println(equipage.toString());
		else
			System.out.println("La taille de l'�quipage 2 d�passe la capacit� de l'�quipage courant.");
		
		System.out.println(equipage.getNombreMarins() + equipage.toString());
		equipage.clear();
		System.out.println("Equipage cleared " + equipage.toString());
		
		if(equipage.equals(equipage2))
			System.out.println("Les deux equipages sont �gaux.");
		else
			System.out.println("Les deux equipages ne sont pas �gaux.");
		
		Equipage equipage3 = new Equipage();
		if(!equipage3.addMarin(new Marin("nom7", "prenom7", 7000)))
			System.out.println("Marin existe dej� ou equipage est plein.");
		if(!equipage3.addMarin(new Marin("nom6", "prenom6", 6000)))
			System.out.println("Marin existe dej� ou equipage est plein.");
		
		System.out.println(equipage2.toString());
		System.out.println(equipage3.toString());
		
		if(equipage2.equals(equipage3))
			System.out.println("Les deux equipages sont �gaux.");
		else
			System.out.println("Les deux equipages ne sont pas �gaux.");
		
		System.out.println(equipage2.hashCode());
		System.out.println(equipage3.hashCode());
		
	}

}
