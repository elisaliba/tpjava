package com.elisesaliba.java.tp.serie4.exo7;

import java.util.HashSet;
import java.util.Set;

import com.elisesaliba.java.tp.serie1.exo4.Marin;

public class TestSet {

	public static void main(String[] args) {
		
		Marin m1, m2, m3;
		Set<Marin> marinsSet;
		
		m1 = new Marin("Nom12", "Prenom12", 1000);
		m2 = new Marin("Nom12", "Prenom12", 1000);
		m3 = new Marin("Nom3", "Prenom3", 3000);
		
		marinsSet = new HashSet<Marin>();
	
		System.out.println("Add m1 : " + marinsSet.add(m1));
		System.out.println("Add m2 : " + marinsSet.add(m2));
		System.out.println("Add m3 : " + marinsSet.add(m3));
		
		System.out.println(m1.hashCode());
		System.out.println(m2.hashCode());
		
		// dans le cas o� equals() et hashCode() ne sont pas surcharg�es,
		// l'ajout de deux marins egaux dans le HashSet ne pose aucun probl�me,
		// par contre, surcharger ces deux methodes nous emp�che d'ajouter les
		// memes marins dans un HashSet. D'o� l'importance de surcharger equals()
		// et HashCode() pour pouvoir comparer le contenu des marins et non pas
		// leurs adresses afin de respecter la s�mantique du Set<Marin>.
	}

}
