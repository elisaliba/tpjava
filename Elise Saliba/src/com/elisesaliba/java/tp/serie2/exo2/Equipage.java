package com.elisesaliba.java.tp.serie2.exo2;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import com.elisesaliba.java.tp.serie1.exo4.Marin;

public class Equipage {

	/*
	private final int MAX = 5;	 
	private Marin[] marins;

	public Equipage () {
		marins = new Marin[MAX];
	}
	public int getCapacite(){
		return marins.length;
	}

	public int getNombreMarins(){
		int nombre = this.getCapacite();

		for(int i = 0; i < this.getCapacite(); i++){
			if(marins[i] == null)
				nombre--;
		}
		return nombre;
	}

	public boolean addMarin(Marin marin){
		if(this.getNombreMarins() == this.getCapacite() || this.isMarinPresent(marin))
			return false;
		else {
			for(int i = 0; i < this.getCapacite(); i++){
				if(marins[i] == null){
					marins[i] = marin;
					break;
				}
			}
			return true;
		}
	}

	@Override
	public String toString() {
		String str = "";

		for(int i = 0; i < this.getCapacite(); i++)
			if(marins[i] != null)
				str += (i+1) + " - " + marins[i].toString() + "\n";
		return str;
	}

	public boolean isMarinPresent(Marin marin){
		for (int i = 0; i < this.getCapacite(); i++){
			if(marins[i]!= null)
				if(marins[i].getNom() == marin.getNom() && marins[i].getPrenom() == marin.getPrenom())
					return true;
		}
		return false;
	}

	public boolean removeMarin(Marin marin){
		if(!isMarinPresent(marin))
			return false;
		else {
			for (int i = 0; i < this.getCapacite(); i++){
				if(marins[i]!= null)
					if(marins[i].getNom() == marin.getNom() && marins[i].getPrenom() == marin.getPrenom())
						marins[i] = null;
			}
			return true;
		}
	}

	public Marin[] getEquipage() {
		int j = 0;
		Marin[] copieMarins = new Marin[this.getNombreMarins()];
		for (int i = 0; i < this.getCapacite(); i++){
			if(marins[i]!= null){
				copieMarins[j] = marins[i];
				j++;
			}
		}
		return copieMarins;
	}

	public void clear(){
		for (int i = 0; i < this.getCapacite(); i++){
			marins[i] = null;
		}
	}

	public boolean addAllEquipage(Equipage equipage){
		int n = 0;
		for(int i = 0; i < equipage.getNombreMarins(); i++) {
			if(!this.isMarinPresent(equipage.getEquipage()[i]))
				n++;
		}
		if(n > (this.getCapacite()-this.getNombreMarins()))
			return false;
		else {
			for(int i = 0; i < equipage.getNombreMarins(); i++){
				this.addMarin(equipage.getEquipage()[i]);
			}
		}
		return true;
	}

	public void etendEquipage(int capacite){
		Marin[] temp = new Marin[this.getCapacite()];
		for(int i = 0; i < this.getCapacite(); i++){
			temp[i] = marins[i];
		}

		marins = new Marin[this.getCapacite() + capacite];

		for(int i = 0; i < temp.length; i++){
			marins[i] = temp[i];
		}

	}

	public boolean equals(Equipage equipageAutre){
		if(this.getNombreMarins() != equipageAutre.getNombreMarins())
			return false;

		for(int i = 0; i < equipageAutre.getNombreMarins(); i++)
			if(!this.isMarinPresent(equipageAutre.getEquipage()[i]))
				return false;
		return true;
	}
	
	public int hashCode() {
		int hashCode = 17;
		int[] marinsHashCode = new int[this.getNombreMarins()];
		for(int i = 0; i < this.getNombreMarins(); i++)
			marinsHashCode[i] = this.getEquipage()[i].hashCode();
		
		Arrays.sort(marinsHashCode);
		
		System.out.println(Arrays.toString(marinsHashCode));
		
		for(Integer i : marinsHashCode)
			hashCode = 31 * hashCode + i;
		
		return hashCode ;
	}
	*/
	
	// S�rie 4 exercice 9
		 
	private SortedMap<String, Marin> mapMarins;
	
	public Equipage () {
		mapMarins = new TreeMap<String, Marin>() ;
	}

	public int getNombreMarins(){
		return mapMarins.size();
	}

	public boolean addMarin(Marin marin){
		if(mapMarins.put(marin.getNom(), marin)==null)
			return false;
		else
			return true;
	}

	@Override
	public String toString() {
		String str = "";
		for (Map.Entry<String, Marin> entry : mapMarins.entrySet()) 
			str+= entry.getValue().toString() + "\n" ;
		
		return str;
	}

	public boolean isMarinPresent(Marin marin){
		return mapMarins.containsValue(marin);
	}

	public boolean removeMarin(Marin marin){
		if(mapMarins.remove(marin)==null)
			return false;
		else
			return true;
	}

	public SortedMap<String, Marin> getEquipage() {
		SortedMap<String, Marin> copieMarins = new TreeMap<>();
		copieMarins.putAll(mapMarins);
		
		return copieMarins;
	}

	public void clear(){
		mapMarins.clear();
	}

	public SortedMap<String, Marin> addAllEquipage(Equipage equipage){
		mapMarins.putAll(equipage.getEquipage());
		
		return mapMarins;
	}

	public boolean equals(Equipage equipageAutre){
		return this.getEquipage().keySet().equals(equipageAutre.getEquipage().keySet());
	}
	
	public Marin getMarinByName(String nom){
		return this.getEquipage().get(nom);
	}
	
}
