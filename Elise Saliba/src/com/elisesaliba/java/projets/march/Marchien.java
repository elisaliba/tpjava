package com.elisesaliba.java.projets.march;

import java.util.Random;

public class Marchien {
	private final int LARGEUR = 50;
	private final int HAUTEUR = 50;
	private int x, y, poids;
	private int direction;
	protected int porteeVisuelle;
	
	public Marchien() {
		Random randDirection = new Random();
		poids = 10;
		Random rand = new Random();
		direction = 1 + randDirection.nextInt(8);
		x = rand.nextInt(LARGEUR);
		y = rand.nextInt(HAUTEUR);
		porteeVisuelle = 3;
	}

	public void deplace(){
		Random randDirection = new Random();
		int newDirection = 1 + randDirection.nextInt(8);
		switch (newDirection){
		case 1 :
			y-= 1;
			if(y<0)
				y = HAUTEUR-1;
			break;
		case 2 :
			x+= 1;
			y-= 1;
			if(x>LARGEUR-1)
				x = 0;
			if(y<0)
				y = HAUTEUR-1;
			break;
		case 3 :
			x+= 1;
			if(x>LARGEUR-1)
				x = 0;
			break;
		case 4 :
			x+= 1;
			y+= 1;
			if(x>LARGEUR-1)
				x = 0;
			if(y>HAUTEUR-1)
				y = 0;
			break;
		case 5 :
			y+= 1;
			if(y>HAUTEUR-1)
				y = 0;
			break;
		case 6 :
			x-= 1;
			y+= 1;
			if(x<0)
				x = LARGEUR-1;
			if(y>HAUTEUR-1)
				y = 0;
			break;
		case 7 :
			x-= 1;
			if(x<0)
				x = LARGEUR-1;
			break;
		case 8 :
			x-= 1;
			y-= 1;
			if(x<0)
				x = LARGEUR-1;
			if(y<0)
				y = HAUTEUR-1;
			break;
		default :
			x = 0;
			y = 0;
			break;
		}
		direction = newDirection;
	}
	
	public void mange(){
		poids += 2;
	}
	
	public Marchien divise(){
		poids = 10;
		return new Marchien();
	}

	public int getPoids() {
		return poids;
	}
	
	public void setPoids(int poids) {
		this.poids = poids;
	}
	
	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public boolean equals(Object obj) {
		if (getClass() != obj.getClass())
			return false;
		Marchien marchien = (Marchien) obj;
		if (x != marchien.x)
			return false;
		if (y != marchien.y)
			return false;
		return true;
	}
	
	public String toString(){
		return "[Marchien : Position : (" + x + "," + y + "), Direction : " + direction
				+ ", Poids : " + poids +  "]";
	}
}
