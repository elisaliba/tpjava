package com.elisesaliba.java.projets.march;

import java.awt.Point;
import java.util.ArrayList;

public class MarchienCouard extends MarchienOeilDeLynx {

	public MarchienCouard(){
		super();	
	}
	/* not done */ 
	public void deplace() {
		int x = super.getX();
		int y = super.getY();
		ArrayList<Point> marchiensTueurs = new ArrayList<>();
		for(int i = 0; i < super.porteeVisuelle; i++){
			for(int j = 1; j <= 8; j++){
				for(int k = 1; k <= 8; k++){
					Point p = new Point(x+j, y+k);
					if(PlaneteMarch.marchsMap.get(p).getClass() == MarchienTueur.class){
						marchiensTueurs.add(p);
					}
				}
			}
		}
		if(marchiensTueurs.isEmpty())
			super.deplace();
		else {
			int diffX = (int) marchiensTueurs.get(0).getX() - super.getX();
			int diffY = (int) marchiensTueurs.get(0).getY() - super.getY();
			if(diffX < 0){ // il est � gauche
				if(diffY < 0){ // � gauche en haut
					super.setX(super.getX()+1);
					super.setY(super.getY()+1);
				} else if(diffY == 0){ // � gauche
					super.setX(super.getX()+1);
				} else if(diffY > 0){ // � gauche en bas
					super.setX(super.getX()+1);
					super.setY(super.getY()-1);
				}
			}
			else if (diffX > 0){ // il est � droite
				if(diffY < 0){ // � droite en haut
					super.setX(super.getX()-1);
					super.setY(super.getY()+1);
				} else if(diffY == 0){ // � droite
					super.setX(super.getX()-1);
				} else if(diffY > 0){ // � droite en bas
					super.setX(super.getX()-1);
					super.setY(super.getY()-1);
				}
			}
			else {
				if(diffY < 0){ // en haut
					super.setY(super.getY()+1);
				} else if(diffY > 0){ // en bas
					super.setX(super.getY()-1);
				}

			}
		}


	}
}
