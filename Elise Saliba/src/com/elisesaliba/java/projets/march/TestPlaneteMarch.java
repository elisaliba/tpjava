package com.elisesaliba.java.projets.march;

public class TestPlaneteMarch {

	public static void main(String[] args) {
		PlaneteMarch planete = new PlaneteMarch(19, 19, 10, 10);
		
		for(int i= 0; i < 20; i++){
			Marchien marchien = new Marchien();
			planete.addMarchien(marchien);
		}
		MarchienOeilDeLynx marchien = new MarchienOeilDeLynx();
		planete.addMarchien(marchien);
		
		System.out.println("Marchiens avant debut cycle " + planete.getMarchiensVivants());

		for(int i = 1; i < 105; i++){
			System.out.println("Cycle " + i);
			planete.lancer();
			System.out.println(planete.getStatistiques());
		}

	}

}
