package com.elisesaliba.java.projets.march;

public class MarchienTueur extends Marchien {

	public MarchienTueur(){
		super();
	}
	
	public void mange(MarchienOeilDeLynx marchien){
		super.setPoids(super.getPoids() + marchien.getPoids()/2);
	}
}
