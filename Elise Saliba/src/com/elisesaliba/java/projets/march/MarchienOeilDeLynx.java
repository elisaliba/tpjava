package com.elisesaliba.java.projets.march;

public class MarchienOeilDeLynx extends Marchien {

	public MarchienOeilDeLynx(){
		super();
	}

	public void deplace() {
		int[] coord;
		if(super.getPoids() < 8){
			if((coord = searchVeg()) == null)
				super.deplace();
			else{
				super.setX(coord[0]);
				super.setY(coord[1]);
			}
		}
		else
			super.deplace();

	}

	public MarchienOeilDeLynx divise() {
		super.setPoids(10);
		return new MarchienOeilDeLynx();
	}

	public int[] searchVeg(){
		int[][] tabMarch = PlaneteMarch.march;
		int[] coord = new int[2]; 
		int x = super.getX();
		int y = super.getY();

		for(int i = 0; i < porteeVisuelle; i++){
			for(int j = 1; j <= 8; j++){
				for(int k = 1; k <= 8; k++){
					if(((x+j) < tabMarch[0].length) && ((y+k) < tabMarch[1].length) 
							&&(tabMarch[x+j][y+k]==1)){
						coord[0] = x+j;
						coord[1] = y+k;
						return coord;
					}
				}
			}
		}
		return null;
	}
	
	public String toString(){
		return "[MarchienOeilDeLynx : Position : (" + super.getX() + "," 
				+ super.getY() + "), Direction : " + super.getDirection()
				+ ", Poids : " + super.getPoids() +  "]";
	}


}
