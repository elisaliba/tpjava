package com.elisesaliba.java.projets.march;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class PlaneteMarch {
	private final int LARGEUR = 50;
	private final int HAUTEUR = 50;
	private int xEden, yEden, largeurEden, hauteurEden, joursCount, saison, saisonEden;
	private List<Marchien> marchiens;
	public static HashMap<Point, Marchien> marchsMap;
	public static int[][] march;
	private boolean ete;

	public PlaneteMarch(int xEden, int yEden, int largeurEden, int hauteurEden){
		this.xEden = xEden;
		this.yEden = yEden;
		this.largeurEden = largeurEden;
		this.hauteurEden = hauteurEden;
		march = new int[LARGEUR][HAUTEUR];
		marchiens = new ArrayList<>();
		marchsMap = new HashMap<>();
		ete = false;
		joursCount = 0;
	}

	public String getStatistiques(){
		int nbreVegetaux, nbreMarchiens, nbreMarchiensODL, nbreMarchiensTueurs;
		nbreVegetaux = nbreMarchiens = nbreMarchiensODL = nbreMarchiensTueurs = 0;
		for(int i = 0; i < LARGEUR; i++){
			for(int j = 0; j < HAUTEUR; j++)
				if(march[i][j] == 1)
					nbreVegetaux++;
		}
		for(int i = 0; i < marchiens.size(); i++){
			Marchien m = marchiens.get(i);
			if(m.getClass() == Marchien.class)
				nbreMarchiens++;
			else if (m.getClass() == MarchienOeilDeLynx.class)
				nbreMarchiensODL++;
			else if (m.getClass() == MarchienTueur.class)
				nbreMarchiensTueurs++;
			
		}
		return "Quantit� de vegetaux = " + nbreVegetaux + "\n"
		+ "Nombre total de Marchien vivants = " + marchiens.size()
		+ ", nombre de Marchien = " + nbreMarchiens 
		+ ", nombre de MarchienOeilDeLynx = " + nbreMarchiensODL
		+ ", nombre de MarchienTueur = " + nbreMarchiensTueurs
		+ "\n" + marchiens.toString() + "\n ----------------- ";
	}

	public void addMarchien(Marchien marchien){
		marchiens.add(marchien);
		Point p = new Point(marchien.getX(), marchien.getY());
		marchsMap.put(p, marchien);
	}

	public boolean removeMarchien(Marchien marchien){
	//	march[marchien.getX()][marchien.getY()] = 0;
		for(int i = 0; i <marchiens.size(); i++)
			if(marchiens.get(i).equals(marchien)){
				marchiens.remove(marchiens.get(i));
				return true;
			}
		marchsMap.remove(new Point(marchien.getX(), marchien.getY()));
		return false;
	}

	public int getMarchiensVivants(){
		return marchiens.size();
	}
	/* not done */ 
	public void lancer(){
		joursCount++;
		if((joursCount%101)==0)
			ete = !ete;
		for(int i = 0; i < marchiens.size(); i++){
			Marchien m = marchiens.get(i);
			m.deplace();
			if (march[m.getX()][m.getY()] == 1){
				m.mange();
			}
			if(m.getPoids()>= 20){
				addMarchien(m.divise());
			}
			if(marchiens.get(i).getClass()==MarchienTueur.class){
				MarchienTueur mt = (MarchienTueur) marchiens.get(i);
				Point p = new Point(mt.getX(), mt.getY());
				if(marchsMap.get(p).getClass() == MarchienOeilDeLynx.class){
					MarchienOeilDeLynx mol = (MarchienOeilDeLynx) marchsMap.get(p);
					mt.mange(mol);
					removeMarchien(mol);
				}
			}
		}
		
		if(!ete){
			saison = saisonEden = 1;
		} else {
			saison = 5;
			saisonEden = 3;
		}
		
		Set<String> indices = new TreeSet<>();
		indices = getVegPositions(saison, saisonEden);

		for(String s : indices) {
			String[] Sp;
			Sp = s.split(",");
			march[Integer.parseInt(Sp[0])][Integer.parseInt(Sp[1])] = 1; 
		}

		for(int i = 0; i < LARGEUR; i++)
			for(int j = 0; i < HAUTEUR; i++)
				if(march[i][j] == 2)
					march[i][j] = 0;

		for(int i = 0; i < marchiens.size(); i++){
			march[marchiens.get(i).getX()][marchiens.get(i).getY()] = 2;
			marchiens.get(i).setPoids(marchiens.get(i).getPoids()-1);
			if(marchiens.get(i).getPoids() <= 0){
				removeMarchien(marchiens.get(i));
			}
		}
		
		marchsMap.clear();

	}

	private Set<String> getVegPositions(int saison, int saisonEden) {
		// Ajout de la vegetation
		Set<String> indices = new TreeSet<>();
		Random rand = new Random();
		int nbreVeg = saison*LARGEUR*HAUTEUR/100;
		int nbreVegEden = saisonEden*largeurEden*hauteurEden/100;
		
		do {
			int i1 = rand.nextInt(LARGEUR);
			int i2 = rand.nextInt(HAUTEUR);
			indices.add(i1+","+i2);
		} while(indices.size()!=(nbreVeg/2));

		do {
			int i1 = xEden + rand.nextInt(xEden+largeurEden+1);
			int i2 = yEden + rand.nextInt(yEden+hauteurEden);
			indices.add(i1+","+i2);
		} while(indices.size()!=((nbreVeg+nbreVegEden)/2));

		return indices;
	}



}