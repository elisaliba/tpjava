package com.elisesaliba.exo1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

public class XMLUtil {
	
	public Document serialize (Marin marin)
	{
		Document document = DocumentHelper.createDocument() ;
		// question b : le noeud racine est "marin"
		Element root = document.addElement("marin");
		// question d : ajouter un attribut au noeud racine
		root.addAttribute("id",  "12"); 
		// question c : ajouter un noeud fils (nom) au noeud racine (marin)
		Element nom = root.addElement("nom");
		// question e : ajouter un contenu textuel au noeud (nom)
		nom.addText("Surcouf"); 
		
		Element prenom = root.addElement("prenom");
		prenom.addText("Robert"); 
		
		Element age = root.addElement("age");
		age.addText("33");
		
		return document;
	}
	
	public void write (Document document, File file) throws IOException
	{
		OutputStream out = new FileOutputStream(file);
		OutputFormat format = OutputFormat.createPrettyPrint();
		XMLWriter writer =  new XMLWriter(out, format);
		writer.write(document);
		writer.flush() ;
	}
	
	public Document read (File file) throws DocumentException
	{
		SAXReader reader =  new SAXReader() ;
		Document document = reader.read(file) ;
		return document;
	}
	
	public Marin deserialize (Document document)
	{
		String id = "", nom = "", prenom = "", age = "";
		
		Element root = document.getRootElement();
		List attributes = root.attributes();
		// question b : verifier que le root contient un attribut
		if (attributes != null)
		{
			Attribute attribute = (Attribute) attributes.get(0);
			String idNom = attribute.getName();
			// question c : obtenir la valeur de l'attribut
			id = attribute.getValue();
		}
		// question d : obtenir la liste des sous-elements de l'element root
		List elements = root.elements() ;
		
		// get first element
		Element nomElement = (Element) elements.get(0);
		
		// question a : connaitre le nom d'un element, getQName pour avoir le nom complet (avec le namespace)
		String nomName = nomElement.getName();
		nom = nomElement.getStringValue();
		
		// get second element
		Element prenomElement = (Element) elements.get(1);
		
		// question e : obtenir le contenu d'un element
		prenom = prenomElement.getStringValue();
		
		// get third and last element
		Element ageElement = (Element) elements.get(2);
		age = ageElement.getStringValue();
		
		Marin marin = new Marin(Long.parseLong(id), nom, prenom, Integer.parseInt(age));
		
		return marin;
	}

}
