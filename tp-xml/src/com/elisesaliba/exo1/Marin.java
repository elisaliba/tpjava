package com.elisesaliba.exo1;

public class Marin {
	private String nom, prenom;
	private int age;
	private long id;
	
	public Marin(long id, String nom, String prenom, int age){
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
	}
	
	public void setId (long id){
		this.id = id;
	}

	public long getId(){
		return id;
	}
	
	public void setNom(String nom){
		this.nom = nom;
	}

	public String getNom(){
		return nom;
	}

	public void setPrenom(String prenom){
		this.prenom = prenom;
	}

	public String getPrenom(){
		return prenom;
	}

	public void setAge(int age){
		this.age = age;
	}

	public int getAge(){
		return age;
	}
	
	public String toString() {
		return "Marin [id = " + id + ", nom=" + nom + ", prenom=" + prenom + ", age=" + age + "]";
	}
}
