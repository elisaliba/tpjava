package com.elisesaliba.exo1;

import java.io.File;
import java.io.IOException;

import org.dom4j.Document;

public class Exo1 {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		// question a : creation d'une instance de la classe XMLUtil
		XMLUtil xmlUtil = new XMLUtil();
		Marin marin = new Marin(1, "Saliba", "Elise", 22);
		Document document = xmlUtil.serialize(marin);
		File file = new File("marin-xml.xml");
		xmlUtil.write(document, file);
		
		Marin marin2 = xmlUtil.deserialize(document);
		System.out.println(marin2.toString());
	}

}
