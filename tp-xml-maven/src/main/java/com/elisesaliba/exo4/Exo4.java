package com.elisesaliba.exo4;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class Exo4 {

	public static void main(String[] args) throws Exception, IOException {
		// TODO Auto-generated method stub
		// Question 1 : url du fichier WSDL des Web Services Amazon
		String url = "https://webservices.amazon.com/AWSECommerceService/AWSECommerceService.wsdl";
		// Question 2 : noeud racine : 'definitions', espace de nom par défaut : "http://schemas.xmlsoap.org/wsdl/"
		// Question 3 : espace de nom Amazon : "http://webservices.amazon.com/AWSECommerceService/2011-08-01", 
		// 				préfixe : 'tns'
		RSSReader reader = new RSSReader();
		InputStream inputStream = reader.read(url);
		Handler handler = new Handler();
		
		
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setValidating(true);
		factory.setNamespaceAware(true);
		SAXParser saxParser = factory.newSAXParser();
		
		saxParser.parse(inputStream, handler);
		
		System.out.println("Le nombre de sous-éléments dans 'types/schema' : " + handler.countElement);
	}

}
