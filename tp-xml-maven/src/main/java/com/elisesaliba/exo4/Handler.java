package com.elisesaliba.exo4;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class Handler extends DefaultHandler{
	boolean insideTypes = false;
	boolean insideSchema = false;
	int countElement = 0;
	boolean firstElementTypes = true;
	boolean firstElementSchema = true;
	
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		// TODO Auto-generated method stub
		super.characters(ch, start, length);
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		// TODO Auto-generated method stub
		super.endElement(uri, localName, qName);
		if (insideTypes) {
			insideTypes = false;
			firstElementTypes = false;
		}
		if (insideSchema) {
			insideSchema = false;
			firstElementSchema = false;
		}
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		// TODO Auto-generated method stub
		super.startElement(uri, localName, qName, attributes);
		if (qName.equals("types")){
			insideTypes = true;
		}
		if (qName.equals("schema")){
			insideSchema = true;
		}
		if (firstElementTypes && firstElementSchema)
			countElement++;
		
	}

}
