package com.elisesaliba.exo3;

import java.awt.List;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.dom4j.Attribute;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class Exo3 {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		
		String url = "https://www.voxxed.com/feed/";
		RSSReader reader = new RSSReader();
		InputStream inputStream = reader.read(url);
		Handler handler = new Handler();
		
		
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setValidating(true);
		factory.setNamespaceAware(true);
		SAXParser saxParser = factory.newSAXParser();
		
		saxParser.parse(inputStream, handler);
		
		System.out.println("Le nom de l'élément racine est : " + handler.qNameList.getItem(0));
		System.out.println("Le nombre de sous éléments est : " + (handler.count - 1));
		System.out.println("Le nombre des éléments 'item' est : " + (handler.countItem));
		System.out.println("Les titres des éléments 'item' sont : " + (handler.titleList.toString()));
		System.out.println("Les titres des éléments ayant comme catégorie 'Cloud' sont : " + (handler.titleListCloud.toString()));
		System.out.println("Les titres des éléments ayant comme catégorie 'IoT' sont : " + (handler.titleListIoT.toString()));
	}

}
