package com.elisesaliba.exo3;

import java.awt.List;
import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class Handler extends DefaultHandler {
	List qNameList = new List();
	ArrayList<String> titleList = new ArrayList<>();
	ArrayList<String> titleListCloud = new ArrayList<>();
	ArrayList<String> titleListIoT = new ArrayList<>();
	int count = 0;
	int countItem = 0;
	boolean insideTitle = false;
	boolean insideItem = false;
	boolean insideCategory = false;
	String title = "";
	
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		// TODO Auto-generated method stub
		super.endElement(uri, localName, qName);
		if (qName.equals("item")){
			insideItem = false;
		}
		if (qName.equals("title") && insideItem) {
			insideTitle = false;
		}
		if (qName.equals("category")){
			insideCategory = false;
		}
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		// TODO Auto-generated method stub
		super.startElement(uri, localName, qName, attributes);
		
		qNameList.add(qName);
		count++;
		if (qName.equals("item")){
			countItem++;
			insideItem = true;
		}
		if (qName.equals("title") && insideItem){
			insideTitle = true;
		}
		if (qName.equals("category")){
			insideCategory = true;
		}
		
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		// TODO Auto-generated method stub
		super.characters(ch, start, length);
		String text = new String(ch, start, length);
		
		if (insideTitle){
			titleList.add(text);
			title = text;
		}
		if (insideCategory){
			if(text.contains("Cloud"))
				titleListCloud.add(title);
			if(text.contains("IoT"))
				titleListIoT.add(title);
		}
	}

}
