package com.elisesaliba.exo3;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

public class RSSReader {

	public InputStream read (String url) throws UnsupportedOperationException, IOException
	{
		HttpClient httpClient = HttpClientBuilder.create().build();
		
		HttpGet get = new HttpGet (url);
		
		HttpResponse response = httpClient.execute(get);
		
		StatusLine statusLine = response.getStatusLine();
		int statusCode = statusLine.getStatusCode();
		
		if(statusCode != HttpStatus.SC_OK)
		{
			System.err.println("Method failed : " + statusLine);
			return null;
		}
		InputStream is = response.getEntity().getContent();
		return is;
	}
}
